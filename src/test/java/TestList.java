import PageObject.HoverPage;
import PageObject.LoginPage;
import PageObject.SecurePage;
import PageObject.TablesPage;
import com.codeborne.selenide.Condition;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static Utils.SetUpConfBrowser.setUpConfBrowser;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestList {

    LoginPage loginPage = new LoginPage();
    SecurePage securePage = new SecurePage();
    HoverPage hoverPage = new HoverPage();
    TablesPage tablesPage = new TablesPage();

    private final String login = "tomsmith";
    private final String password = "SuperSecretPassword!";

    @BeforeAll
    public static void setUp() {
        setUpConfBrowser();
        open();
    }

    @AfterAll
    public static void closeUp() {
        closeWebDriver();
    }

    @Test
    @DisplayName("Задание 1 - Корректный логин")
    public void test001(){
        open(Enviroment.LOGIN.getUrl());
        assertEquals(Enviroment.LOGIN.getUrl(),url());
        loginPage.setUsername(login);
        loginPage.setPassword(password);
        loginPage.clickLoginButton();
        assertEquals("http://the-internet.herokuapp.com/secure",url());
        securePage.getLable();
    }

    @Test
    @DisplayName("Задание 2 - Некорректный логин")
    public void test002(){
        open(Enviroment.LOGIN.getUrl());
        assertEquals(Enviroment.LOGIN.getUrl(),url());
        loginPage.setUsername("login");
        loginPage.setPassword(password);
        loginPage.clickLoginButton();
        $(byText("Your username is invalid!")).shouldBe(Condition.visible);
    }

    @Test
    @DisplayName("Задание 3 - Некорректный пароль")
    public void test003(){
        open(Enviroment.LOGIN.getUrl());
        assertEquals(Enviroment.LOGIN.getUrl(),url());
        loginPage.setUsername(login);
        loginPage.setPassword("password");
        loginPage.clickLoginButton();
        $(byText("Your password is invalid!")).shouldBe(Condition.visible);
    }

    @Test
    @DisplayName("Задание 4 - Отображение имен юзеров по наведению на аватар")
    public void test004(){
        open(Enviroment.HOVER.getUrl());
        assertEquals(Enviroment.HOVER.getUrl(),url());
        hoverPage.getFigures(0).hover();
        assertEquals(hoverPage.checkUserName(0),true);
        hoverPage.getFigures(1).hover();
        assertEquals(hoverPage.checkUserName(1),true);
        hoverPage.getFigures(2).hover();
        assertEquals(hoverPage.checkUserName(2),true);
    }

    @Test
    @DisplayName("Задание 5* - Сортировка таблицы")
    public void test005(){
        open(Enviroment.TABLES.getUrl());
        assertEquals(Enviroment.TABLES.getUrl(),url());
        tablesPage.getLastName();
        tablesPage.clickForSort();
        tablesPage.getLastName();
        tablesPage.clickForSort();
        tablesPage.getLastName();
    }
}
