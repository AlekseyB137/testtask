public enum Enviroment {

    LOGIN("http://the-internet.herokuapp.com/login"),
    HOVER("http://the-internet.herokuapp.com/hovers"),
    TABLES("http://the-internet.herokuapp.com/tables");

    private final String url;

    Enviroment(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }
}
