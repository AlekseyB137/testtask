package Utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.By.cssSelector;

public class SetUpConfBrowser {

    public static void setUpConfBrowser(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.headless = true;
        Configuration.timeout = 5000;
        Configuration.savePageSource = false;
        Configuration.fastSetValue = true;
        Configuration.reportsFolder = "test-result/reports";
    }
}

