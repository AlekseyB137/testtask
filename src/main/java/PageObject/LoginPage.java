package PageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;

public class LoginPage {

    private final SelenideElement username = $(id("username"));
    private final SelenideElement password = $(id("password"));
    private final SelenideElement loginButton = $(cssSelector("[type='submit']"));

    public void setUsername(String name){
        username.shouldBe(Condition.visible).setValue(name);
    }

    public void setPassword(String pass){
        password.shouldBe(Condition.visible).setValue(pass);
    }

    public void clickLoginButton(){
        loginButton.shouldBe(Condition.visible).click();
    }
}
