package PageObject;

import com.codeborne.selenide.SelenideElement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;

public class HoverPage {

    private List<String> username = Arrays.asList("user1","user2","user3");
    private List<SelenideElement> figures = $$(className("figure"));

    public boolean checkUserName(int i) {

        List<String> usernameFromSite = $$(cssSelector(".figure .figcaption h5"))
                                        .stream()
                                        .map(SelenideElement::getOwnText)
                                        .map(s -> s.replace("name: ",""))
                                        .collect(Collectors.toList());

        return username.get(i).equals(usernameFromSite.get(i));
    }

    public SelenideElement getFigures(int i) {
        return figures.get(i);
    }
}
