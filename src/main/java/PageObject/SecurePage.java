package PageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.id;

public class SecurePage {

    private final SelenideElement lable = $(id("flash"));
    private final String header = "You logged into a secure area is present";

    public void getLable(){
        lable.shouldBe(Condition.visible);
        $(byText(header)).shouldBe(Condition.visible);
    }

}
