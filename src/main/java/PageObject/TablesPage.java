package PageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.By.cssSelector;

public class TablesPage {

    private final SelenideElement nameFieldLastName = $(cssSelector("#table2 .header .last-name"));

    public void clickForSort() {
        nameFieldLastName.shouldBe(Condition.visible).click();
    }

    public void getLastName() {
        List<String> lastName = $$(cssSelector("#table2 td.last-name")).stream().map(SelenideElement::getOwnText).collect(Collectors.toList());
        List<String> sortedList = lastName.stream().sorted().collect(Collectors.toList());
        List<String> reverseList = lastName.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        if (lastName.equals(sortedList)) System.out.println("Ascending");
        else if (lastName.equals(reverseList)) System.out.println("Descending");
        else System.out.println("Not sorted!");
    }
}

